package com.sic.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sic.model.DetalhesErro;
import com.sic.services.exceptions.AcessoNegadoException;
import com.sic.services.exceptions.ErroMicroServicoException;
import com.sic.services.exceptions.ServicoIndisponivelException;

@ControllerAdvice
public class ResourceExceptionHandler {
	
	@ExceptionHandler(AcessoNegadoException.class)
	public ResponseEntity<DetalhesErro> HandlerAcessoNegado(AcessoNegadoException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(401l);
		erro.setTitulo("Acesso não autorizado.");
		erro.setMensagemDesenvolvedor("Acesso não autorizado, usuário e/ou senha inválida.");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(erro);
	}
	
	@ExceptionHandler(UsernameNotFoundException.class)
	public ResponseEntity<DetalhesErro> HandlerUsernameNotFoundException(UsernameNotFoundException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(404l);
		erro.setTitulo("Usuário e/ou senha não informados.");
		erro.setMensagemDesenvolvedor("Usuário e/ou senha não informados.");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(erro);
	}
	
	@ExceptionHandler(ErroMicroServicoException.class)
	public ResponseEntity<DetalhesErro> HandlerUsernameNotFoundException(ErroMicroServicoException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(500l);
		erro.setTitulo("Erro micro serviço. Entre em contato com o suporte!");
		erro.setMensagemDesenvolvedor("Erro micro serviço. Entre em contato com o suporte!");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(erro);
	}
	
	@ExceptionHandler(ServicoIndisponivelException.class)
	public ResponseEntity<DetalhesErro> HandlerUsernameNotFoundException(ServicoIndisponivelException e, HttpServletRequest request){
		
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(502l);
		erro.setTitulo("Serviço indisponível!");
		erro.setMensagemDesenvolvedor("Micro serviço indisponível.");
		erro.setTimestamp(System.currentTimeMillis());
		
		return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(erro);
	}
}