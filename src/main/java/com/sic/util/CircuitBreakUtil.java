package com.sic.util;

import java.io.Serializable;
import java.util.Objects;

import org.springframework.util.StringUtils;

import com.sic.services.exceptions.ErroMicroServicoException;
import com.sic.services.exceptions.ServicoIndisponivelException;

public class CircuitBreakUtil implements Serializable {

	private static final long serialVersionUID = -2844857645902474697L;

	public static void trataErro(Exception e) {
		if(Objects.nonNull(e)) {
			if(!StringUtils.isEmpty(e.getMessage()) && e.getMessage().contains("ConnectException")) {
				throw new ServicoIndisponivelException("Serviço indisponível.");
			}else {
				throw new ErroMicroServicoException("Erro micro service.");
			}
		}
	}
}
