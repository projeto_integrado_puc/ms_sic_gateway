package com.sic.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CriptoUtil implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String s = "2022tccSic";

		/*MessageDigest algorithm = MessageDigest.getInstance("MD5");
        byte messageDigest[] = algorithm.digest(s.getBytes("UTF-8"));**/
        System.out.println(encriptPassword(s));
	}
	
	public static String encriptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest messageDigest =  MessageDigest.getInstance("SHA-256");
        messageDigest.update(password.getBytes("UTF-8"));
        return new BigInteger(1, messageDigest.digest()).toString(16);
    }
}