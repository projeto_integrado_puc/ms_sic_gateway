package com.sic.services;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.model.dto.CotacaoFinalizarDTO;
import com.sic.model.dto.CotacaoVencedoraDTO;
import com.sic.model.dto.ProdutoDTO;
import com.sic.model.dto.wscotacao.CotacaoDTO;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class CotacaoService implements Serializable{

	private static final long serialVersionUID = 8251967761747289564L;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${url.cotacao}")
	private String urlCotacao;
	
	@CircuitBreaker(name="gravarCotacao", fallbackMethod = "fallBackCotacao")
	public ResponseEntity gravar(CotacaoDTO cotacao) {
		String url = urlCotacao + "cotacao";
		
		ResponseEntity<String> re = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<CotacaoDTO>(cotacao, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), String.class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<ProdutoDTO>());
		}
	}
	
	@CircuitBreaker(name="loadCotacaoByCnpj", fallbackMethod = "fallBackCotacao")
	public ResponseEntity<List<CotacaoDTO>> loadCotacaoByCnpj(String cnpj, Integer page) {
		String url = urlCotacao + "cotacao/load/" + cnpj + "/" + page;
		
		ResponseEntity<CotacaoDTO[]> re = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<CotacaoDTO[]>(createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), CotacaoDTO[].class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<CotacaoDTO>());
		}
	}
	
	@CircuitBreaker(name="loadCotacao", fallbackMethod = "fallBackCotacao")
	public ResponseEntity<List<CotacaoDTO>> loadCotacao(Integer page) {
		String url = urlCotacao + "cotacao/load/" + page;
		
		ResponseEntity<CotacaoDTO[]> re = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<CotacaoDTO[]>(createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), CotacaoDTO[].class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<CotacaoDTO>());
		}
	}
	
	
	@CircuitBreaker(name="finalizaCotacao", fallbackMethod = "fallBackCotacao")
	public ResponseEntity<CotacaoVencedoraDTO> finalizaCotacao(CotacaoFinalizarDTO cotacaoFinalizarDTO) {
		String url = urlCotacao + "cotacao";
		
		ResponseEntity<CotacaoVencedoraDTO> re = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<CotacaoFinalizarDTO>(cotacaoFinalizarDTO, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), CotacaoVencedoraDTO.class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(re.getBody());
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
	}
	
	HttpHeaders createHeaders(String username, String password){
		   return new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }};
		}
	
	public ResponseEntity fallBackCotacao(Throwable e) {
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}		
}
