package com.sic.services;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.model.dto.FornecedorPropostaDTO;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class PropostaService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${url.cotacao}")
	private String urlCotacao;

	@CircuitBreaker(name = "gravarCotacao", fallbackMethod = "fallBackProposta")
	public ResponseEntity<String> gravar(List<FornecedorPropostaDTO> propostas) {
		String url = urlCotacao + "proposta/grava";

		ResponseEntity<String> re = restTemplate.exchange(url, HttpMethod.POST,
				new HttpEntity<List<FornecedorPropostaDTO>>(propostas,
						createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")),
				String.class);

		if (Objects.nonNull(re)) {
			return ResponseEntity.ok(re.getBody());
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
	}

	@CircuitBreaker(name = "loadProposta", fallbackMethod = "fallBackProposta")
	public ResponseEntity<FornecedorPropostaDTO[]> loadProposta(String cnpj, Long idCotacao, boolean usuarioAdmin) {
		String url = urlCotacao + "proposta/" + cnpj + "/" + idCotacao + "/" + usuarioAdmin;

		ResponseEntity<FornecedorPropostaDTO[]> re = restTemplate.exchange(url, HttpMethod.GET,
				new HttpEntity<FornecedorPropostaDTO[]>(createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")),
				FornecedorPropostaDTO[].class);

		if (Objects.nonNull(re)) {
			return ResponseEntity.ok(re.getBody());
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		}
	}

	HttpHeaders createHeaders(String username, String password) {
		return new HttpHeaders() {
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String(encodedAuth);
				set("Authorization", authHeader);
			}
		};
	}

	public ResponseEntity fallBackProposta(Throwable e) {
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Serviço indisponível.");
	}
}
