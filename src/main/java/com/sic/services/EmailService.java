package com.sic.services;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.model.dto.Email;
import com.sic.model.dto.EmailRequest;
import com.sic.model.dto.ProdutoDTO;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class EmailService implements Serializable{

	private static final long serialVersionUID = 8251967761747289564L;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${url.email}")
	private String urlEmail;
	
	@CircuitBreaker(name="emailSimples", fallbackMethod = "fallBackEmail")
	public ResponseEntity envioEmailSimples(Email email) {
		String url = urlEmail + "email/envio";
		
		ResponseEntity re = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<Email>(email, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), Void.class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<ProdutoDTO>());
		}
	}
	
	@CircuitBreaker(name="envioListaEmailSimples", fallbackMethod = "fallBackEmail")
	public ResponseEntity envioListaEmailSimples(List<Email> email) {
		String url = urlEmail + "email/lista";
		
		ResponseEntity re = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<List<Email>>(email, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), Void.class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<ProdutoDTO>());
		}
	}
	
	@CircuitBreaker(name="envioCotacao", fallbackMethod = "fallBackEmail")
	public ResponseEntity envioCotacao(EmailRequest request) {
		String url = urlEmail + "email/cotacao";
		
		ResponseEntity re = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<EmailRequest>(request, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), Void.class);

		if(Objects.nonNull(re)) {
			return ResponseEntity.ok(Arrays.asList(re.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<ProdutoDTO>());
		}
	}
	
	HttpHeaders createHeaders(String username, String password){
		   return new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }};
		}
	
	public ResponseEntity fallBackEmail(Throwable e) {
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body("Serviço indisponível.");
	}		
}
