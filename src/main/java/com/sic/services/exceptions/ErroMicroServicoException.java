package com.sic.services.exceptions;

public class ErroMicroServicoException extends RuntimeException {

	private static final long serialVersionUID = -6870067148560374214L;

	public ErroMicroServicoException(String message) {
		super(message);
	}
	
	public ErroMicroServicoException(String message, Throwable causa) {
		super(message, causa);
	}
}
