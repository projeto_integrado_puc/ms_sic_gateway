package com.sic.services.exceptions;

public class ServicoIndisponivelException extends RuntimeException {

	private static final long serialVersionUID = -6870067148560374214L;

	public ServicoIndisponivelException(String message) {
		super(message);
	}
	
	public ServicoIndisponivelException(String message, Throwable causa) {
		super(message, causa);
	}
}
