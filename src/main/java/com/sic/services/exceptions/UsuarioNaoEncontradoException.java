package com.sic.services.exceptions;

public class UsuarioNaoEncontradoException extends RuntimeException {

	private static final long serialVersionUID = 323305792030946880L;

	public UsuarioNaoEncontradoException(String message) {
		super(message);
	}
	
	public UsuarioNaoEncontradoException(String message, Throwable causa) {
		super(message, causa);
	}
}
