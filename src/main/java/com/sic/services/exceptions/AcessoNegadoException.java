package com.sic.services.exceptions;

public class AcessoNegadoException extends RuntimeException {

	private static final long serialVersionUID = -6870067148560374214L;

	public AcessoNegadoException(String message) {
		super(message);
	}
	
	public AcessoNegadoException(String message, Throwable causa) {
		super(message, causa);
	}
}
