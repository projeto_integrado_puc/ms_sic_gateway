package com.sic.services;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.model.dto.ProdutoDTO;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@Service
public class ProdutoService implements Serializable{

	private static final long serialVersionUID = 8251967761747289564L;
	
	private final Log logger = LogFactory.getLog(this.getClass());
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${url.legado}")
	private String urlLegado;
	
	@CircuitBreaker(name="produto", fallbackMethod = "fallBackProduto")
	public ResponseEntity<List<ProdutoDTO>> getProdutosEstoqueAbaixoMinimo() {
		this.logger.info("Acessa o serviço Ms SIC Legado");
		
		String url = urlLegado + "produto";
		
		
		ResponseEntity<ProdutoDTO[]> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<ProdutoDTO[]>(createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), ProdutoDTO[].class);
		
		if(Objects.nonNull(response) && response.getBody() != null && response.getBody().length > 0) {
			return ResponseEntity.ok(Arrays.asList(response.getBody()));
		}else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ArrayList<ProdutoDTO>());
		}
	}
	
	public void updateCotacao(List<Long> produtos) {
		String url = urlLegado + "produto";
		restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<List<Long>>(produtos, createHeaders("sic", "909b5410ee6894deb29e40b24908ee85")), Void.class);
	}
	
	HttpHeaders createHeaders(String username, String password){
		   return new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		      }};
		}
	
	public ResponseEntity<List<ProdutoDTO>> fallBackProduto(Throwable e) {
		this.logger.info("Exceção circuit break.");
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}	
}
