package com.sic.services;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Objects;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sic.services.exceptions.AcessoNegadoException;
import com.sic.util.CriptoUtil;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	
	@Value("${sic.user}")
	private String user;
	
	@Value("${sic.password}")
	private String password;
	
	public UserDetails loadUserByUsernameAndPassword(String username, String senha) throws UsernameNotFoundException, NoSuchAlgorithmException, UnsupportedEncodingException {
		if(Strings.isBlank(username) || Strings.isBlank(senha)) {
			throw new UsernameNotFoundException("Usuário ou senha não informado.");
		}else {
			if (username.equals(user) && CriptoUtil.encriptPassword(senha).equals(password)) {
				return new User(username, senha, new ArrayList<>());
			} else {
				throw new AcessoNegadoException("Usuario não encontrado: " + username);
			}
		}
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (Objects.nonNull(username)){
			return new User(username, "", new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("Usuario não encontrado: " + username);
		}
	}
}