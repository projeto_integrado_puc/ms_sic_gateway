package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.ProdutoDTO;
import com.sic.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoResource {

	@Autowired
	private ProdutoService produtoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ProdutoDTO>> listar() {
		return produtoService.getProdutosEstoqueAbaixoMinimo();
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Void> atualizaItensEmCotacao(@RequestBody List<Long> produtos){
		if(Objects.nonNull(produtos) && produtos.size() > 0) {
			this.produtoService.updateCotacao(produtos);
		}
		return ResponseEntity.ok().build();
	}
}
