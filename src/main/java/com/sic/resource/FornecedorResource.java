package com.sic.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.FornecedorDTO;
import com.sic.services.FornecedorService;

@RestController
@RequestMapping("/fornecedor")
public class FornecedorResource {

	@Autowired
	private FornecedorService fornecedorService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<FornecedorDTO>> listar() {
		return fornecedorService.loadFornecedores();
	}
}
