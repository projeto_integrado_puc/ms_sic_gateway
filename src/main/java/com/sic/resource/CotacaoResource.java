package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.CotacaoFinalizarDTO;
import com.sic.model.dto.CotacaoVencedoraDTO;
import com.sic.model.dto.wscotacao.CotacaoDTO;
import com.sic.services.CotacaoService;

@RestController
@RequestMapping("/cotacao")
public class CotacaoResource {

	@Autowired
	private CotacaoService cotacaoService;

	@RequestMapping(method = RequestMethod.POST, path = "/gravar")
	public ResponseEntity<String> gravar(@RequestBody CotacaoDTO cotacao) {
		return cotacaoService.gravar(cotacao);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/load/{cnpj}/{page}")
	public ResponseEntity<List<CotacaoDTO>> loadCotacaoByCnpj(@PathVariable("cnpj") String cnpj,
			@PathVariable("page") Integer page) {
		if (Objects.isNull(cnpj)) {
			return ResponseEntity.noContent().build();
		} else {
			return this.cotacaoService.loadCotacaoByCnpj(cnpj, page);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/load/{page}")
	public ResponseEntity<List<CotacaoDTO>> loadCotacao(@PathVariable("page") Integer page) {
		return this.cotacaoService.loadCotacao(page);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<CotacaoVencedoraDTO> finalizaCotacao(@RequestBody CotacaoFinalizarDTO cotacaoFinalizarDTO){
		if(Objects.nonNull(cotacaoFinalizarDTO)) {
			return this.cotacaoService.finalizaCotacao(cotacaoFinalizarDTO);
		}else {
			return ResponseEntity.noContent().build();
		}		
	}
}
