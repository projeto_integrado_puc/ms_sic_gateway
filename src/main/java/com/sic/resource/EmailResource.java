package com.sic.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.Email;
import com.sic.model.dto.EmailRequest;
import com.sic.services.EmailService;

@RestController
@RequestMapping("/email")
public class EmailResource {

	@Autowired
	private EmailService emailService;

	@RequestMapping(method = RequestMethod.POST, path = "/envio")
	public ResponseEntity envioEmail(@RequestBody Email email) {
		return emailService.envioEmailSimples(email);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/lista")
	public ResponseEntity envioListaEmail(@RequestBody List<Email> lista) {
		return emailService.envioListaEmailSimples(lista);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/cotacao")
	public ResponseEntity envioListaEmail(@RequestBody EmailRequest email) {
		return emailService.envioCotacao(email);
	}
}
