package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.FornecedorPropostaDTO;
import com.sic.services.PropostaService;

@RestController
@RequestMapping("/proposta")
public class PropostaResource {

	@Autowired
	private PropostaService propostaService;

	@RequestMapping(method = RequestMethod.POST, path = "/grava")
	public ResponseEntity<String> gravar(@RequestBody List<FornecedorPropostaDTO> propostas) {
		return propostaService.gravar(propostas);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/load/{cnpj}/{idCotacao}/{usuarioAdmin}")
	public ResponseEntity<FornecedorPropostaDTO[]> loadProposta(@PathVariable("cnpj") String cnpj, @PathVariable("idCotacao") Long idCotacao, @PathVariable("usuarioAdmin") boolean usuarioAdmin) {
		if (Objects.isNull(cnpj) || Objects.isNull(idCotacao)) {
			return ResponseEntity.noContent().build();
		} else {
			return propostaService.loadProposta(cnpj, idCotacao, usuarioAdmin);
		}
	}
}
