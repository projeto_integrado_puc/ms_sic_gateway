package com.sic.model;

import java.io.Serializable;
import java.util.Date;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String jwttoken;
	private Date expira; 

	public JwtResponse(String username, String jwttoken, Date expira) {
		this.username = username;
		this.jwttoken = jwttoken;
		this.expira = expira;
	}

	public String getUsername() {
		return username;
	}

	public String getToken() {
		return this.jwttoken;
	}

	public Date getExpira() {
		return expira;
	}
	
	
}