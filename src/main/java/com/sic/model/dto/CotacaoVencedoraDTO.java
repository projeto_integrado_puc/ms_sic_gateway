package com.sic.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CotacaoVencedoraDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date dtCotacao;
	private Date fimDtCotacao;
	private List<CotacaoItemDTO> itens;
	private String nomeComprador;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtCotacao() {
		return dtCotacao;
	}
	public void setDtCotacao(Date dtCotacao) {
		this.dtCotacao = dtCotacao;
	}
	public Date getFimDtCotacao() {
		return fimDtCotacao;
	}
	public void setFimDtCotacao(Date fimDtCotacao) {
		this.fimDtCotacao = fimDtCotacao;
	}
	public List<CotacaoItemDTO> getItens() {
		return itens;
	}
	public void setItens(List<CotacaoItemDTO> itens) {
		this.itens = itens;
	}
	public String getNomeComprador() {
		return nomeComprador;
	}
	public void setNomeComprador(String nomeComprador) {
		this.nomeComprador = nomeComprador;
	}
}