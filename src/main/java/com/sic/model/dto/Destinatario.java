package com.sic.model.dto;

import java.io.Serializable;
import java.util.List;

public class Destinatario implements Serializable{

	private static final long serialVersionUID = -5809802192456091581L;
	
	private String email;
	private List<Parametro> parametros;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Parametro> getParametros() {
		return parametros;
	}
	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}
}
