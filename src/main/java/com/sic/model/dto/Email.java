package com.sic.model.dto;

import java.io.Serializable;

import com.sic.model.ResponseGateway;

public class Email extends ResponseGateway implements Serializable {

	private static final long serialVersionUID = 8627375840331309168L;

	private String destinatario;
	private String assunto;
	private String texto;
	
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
}
