package com.sic.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class CotacaoItemDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String codigoProduto;
	private String descricaoProduto;
	private String cnpjFornecedor;
	private String nomeFornecedorVencedor;
	private BigDecimal valor;
	private BigDecimal frete;
	private Date dataEntrega;
	private Long quantidade;
	private String email;

	public String getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getDescricaoProduto() {
		return descricaoProduto;
	}

	public void setDescricaoProduto(String descricaoProduto) {
		this.descricaoProduto = descricaoProduto;
	}

	public String getCnpjFornecedor() {
		return cnpjFornecedor;
	}

	public void setCnpjFornecedor(String cnpjFornecedor) {
		this.cnpjFornecedor = cnpjFornecedor;
	}

	public String getNomeFornecedorVencedor() {
		return nomeFornecedorVencedor;
	}

	public void setNomeFornecedorVencedor(String nomeFornecedorVencedor) {
		this.nomeFornecedorVencedor = nomeFornecedorVencedor;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getFrete() {
		return frete;
	}

	public void setFrete(BigDecimal frete) {
		this.frete = frete;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}