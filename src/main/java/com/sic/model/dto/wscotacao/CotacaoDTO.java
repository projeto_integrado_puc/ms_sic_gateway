package com.sic.model.dto.wscotacao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CotacaoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Date dtCotacao;
	private Date fimDtCotacao;
	private List<ProdutoDTO> produtos;
	private String nomeComprador;
	private long totalRegistros;
	private boolean statusAberto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDtCotacao() {
		return dtCotacao;
	}
	public void setDtCotacao(Date dtCotacao) {
		this.dtCotacao = dtCotacao;
	}
	public Date getFimDtCotacao() {
		return fimDtCotacao;
	}
	public void setFimDtCotacao(Date fimDtCotacao) {
		this.fimDtCotacao = fimDtCotacao;
	}
	public List<ProdutoDTO> getProdutos() {
		return produtos;
	}
	public void setProdutos(List<ProdutoDTO> produtos) {
		this.produtos = produtos;
	}
	public String getNomeComprador() {
		return nomeComprador;
	}
	public void setNomeComprador(String nomeComprador) {
		this.nomeComprador = nomeComprador;
	}
	public long getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(long totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public boolean isStatusAberto() {
		return statusAberto;
	}
	public void setStatusAberto(boolean statusAberto) {
		this.statusAberto = statusAberto;
	}
}
