package com.sic.model.dto;

import java.io.Serializable;

import com.sic.model.ResponseGateway;

public class ProdutoDTO extends ResponseGateway implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private String codigo;
	private boolean emEstoqueMinimo;
	private boolean cotacaoEmAberto;
	private Long quantidade;
	private FornecedorDTO[] fornecedores;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public boolean isEmEstoqueMinimo() {
		return emEstoqueMinimo;
	}

	public void setEmEstoqueMinimo(boolean emEstoqueMinimo) {
		this.emEstoqueMinimo = emEstoqueMinimo;
	}

	public boolean isCotacaoEmAberto() {
		return cotacaoEmAberto;
	}

	public void setCotacaoEmAberto(boolean cotacaoEmAberto) {
		this.cotacaoEmAberto = cotacaoEmAberto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public FornecedorDTO[] getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(FornecedorDTO[] fornecedores) {
		this.fornecedores = fornecedores;
	}
}
