package com.sic.model;

public enum PrioridadeEnum {

	INFORME_PRIORIDADE("Informe Prioridade"),DATA_ENTREGA("Data entrega"), REPUTACAO_FORNECEDOR("Reputação Fornecedor"), VALOR("Valor");

	private String descricao;

	PrioridadeEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
