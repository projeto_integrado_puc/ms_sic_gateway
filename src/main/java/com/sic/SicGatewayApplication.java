package com.sic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SicGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SicGatewayApplication.class, args);
	}

}
